package lk.apiit.eirlss.assignment.repositories;

import lk.apiit.eirlss.assignment.models.Billing;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BillingRepo extends CrudRepository<Billing,String> {
}
