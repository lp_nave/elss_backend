package lk.apiit.eirlss.assignment.repositories;

import lk.apiit.eirlss.assignment.models.Vehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VehicleRepo extends CrudRepository<Vehicle, String> {
}
