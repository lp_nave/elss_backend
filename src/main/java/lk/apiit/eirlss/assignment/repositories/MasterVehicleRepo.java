package lk.apiit.eirlss.assignment.repositories;

import lk.apiit.eirlss.assignment.models.MasterVehicle;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MasterVehicleRepo extends CrudRepository<MasterVehicle,String> {
}
