package lk.apiit.eirlss.assignment.repositories;

import lk.apiit.eirlss.assignment.models.Authentication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthenticationRepo extends CrudRepository<Authentication,String> {
}
