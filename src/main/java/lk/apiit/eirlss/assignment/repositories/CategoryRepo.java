package lk.apiit.eirlss.assignment.repositories;

import lk.apiit.eirlss.assignment.models.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepo extends CrudRepository<Category, String> {
}
