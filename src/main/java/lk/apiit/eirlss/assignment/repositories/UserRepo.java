package lk.apiit.eirlss.assignment.repositories;

import lk.apiit.eirlss.assignment.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepo extends CrudRepository<User,String> {
}
