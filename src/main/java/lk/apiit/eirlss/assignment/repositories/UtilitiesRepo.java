package lk.apiit.eirlss.assignment.repositories;

import lk.apiit.eirlss.assignment.models.Utilities;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilitiesRepo extends CrudRepository<Utilities,String> {
}
