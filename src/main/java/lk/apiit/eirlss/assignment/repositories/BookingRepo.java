package lk.apiit.eirlss.assignment.repositories;

import lk.apiit.eirlss.assignment.models.Booking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepo extends CrudRepository<Booking, String> {
}
