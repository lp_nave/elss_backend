package lk.apiit.eirlss.assignment.repositories;

import lk.apiit.eirlss.assignment.models.ExtendedBookings;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExtendedBookingRepo extends CrudRepository<ExtendedBookings, String> {
}
