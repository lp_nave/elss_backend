package lk.apiit.eirlss.assignment.models;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "user")
public class User {

    @Id
    @Column(name = "userID")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String userID;

    @Column( name="firstName")
    private String firstName;

    @Column(name = "lastName")
    private String lastName;

    @Column(name = "nic")
    private String nic;

    @Column(name = "drivingLicense")
    private String drivingLicense;

    @Column(name = "dob")
    private Date dob;

    @Column(name = "state")
    private String state;

    @Column(name = "contactNumber")
    private String contactNumber;

    @Column(name = "newcomer")
    private boolean newcomer;

    @Column(name = "blacklisted")
    private boolean blacklisted;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "authentication_userName", referencedColumnName = "userName")
    private Authentication authentication;
//
//    @OneToMany
//    private List<Booking> booking;

}
