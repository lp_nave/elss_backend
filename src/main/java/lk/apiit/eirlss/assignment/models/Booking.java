package lk.apiit.eirlss.assignment.models;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "booking")
public class Booking {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "bookingID")
    private String bookingID;

    @Column(name = "pickupTime")
    private Date pickupTime;

    @Column(name = "dropoffTime")
    private Date dropoffTime;

    @Column(name = "active")
    private String active;

    @Column(name = "latedropoff")
    private boolean latedropoff;

    @Column(name = "bookedTime")
    private Date bookedTime;

    @ManyToOne
    @JoinColumn(name = "user_userID", referencedColumnName = "userID")
    private User user;

    @ManyToOne
    @JoinColumn(name = "vehicle_vehcicleID" , referencedColumnName = "masterVehicleID")
    private Vehicle vehicle;

//    @OneToMany
//    private List<Utilities> utilities;
//
//    @OneToMany
//    private List<ExtendedBookings> extendedBookings;
//
    @OneToOne
    private Billing billing;


}
