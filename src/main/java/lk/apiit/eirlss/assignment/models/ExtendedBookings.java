package lk.apiit.eirlss.assignment.models;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "extendedBooking")
public class ExtendedBookings {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "extendedBookingID")
    private String extendedBookingID;

    @Column(name = "extendDropOff")
    private Date extendDropOff;

    @ManyToOne
    @JoinColumn(name = "booking_BookingID", referencedColumnName = "bookingId")
    private Booking bookingID;

}
