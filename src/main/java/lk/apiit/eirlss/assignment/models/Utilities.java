package lk.apiit.eirlss.assignment.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "Utilities")
public class Utilities {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "itemID")
    private String itemID;

    @Column(name = "itemName")
    private String itemName;

    @Column(name = "itemsLeft")
    private int itemsLeft;

    @Column(name = "amount")
    private int amount;

    @ManyToOne
    @JoinColumn(name = "bookingID", referencedColumnName = "bookingID")
    private Booking booking;
}
