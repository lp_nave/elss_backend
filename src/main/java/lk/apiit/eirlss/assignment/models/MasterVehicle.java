package lk.apiit.eirlss.assignment.models;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "master_vehicle")
public class MasterVehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "master_vehicleID")
    private String master_vehicleID;

    @Column(name = "licensePlate")
    private String licensePlate;

    @Column(name = "ussageStatus")
    private String usageStatus;

    @Column(name = "mileage")
    private int mileage;

    @Column(name = "nextServiceDate")
    private Date nextServiceDate;

    @Column(name = "licenseRenewalDate")
    private Date licenseRenewalDate;

    @OneToOne
    private Vehicle vehicle;
}
