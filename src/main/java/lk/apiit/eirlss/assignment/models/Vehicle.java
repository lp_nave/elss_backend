package lk.apiit.eirlss.assignment.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "Vehicle")
public class Vehicle {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "vehicleID")
    private String vehicleID;

    @OneToOne
    @JoinColumn(name = "masterVehicleID", referencedColumnName = "master_vehicleID")
    private MasterVehicle masterVehicleID;

    @ManyToOne
    @JoinColumn(name = "masterCategory", referencedColumnName = "categoryID")
    private Category category;

    @Column(name = "rentStatus")
    private boolean RentStatus;

    @Column(name = "fuelType")
    private String fuelType;

    @Column(name = "transmission")
    private String transmission;

    @Column(name = "make")
    private String make;

    @Column(name = "model")
    private String model;

    @Column(name = "photoURL")
    private String photourl;

    @ManyToOne
    private Booking booking;

}
