package lk.apiit.eirlss.assignment.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "category")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "categoryID")
    private String categoryID;

    @Column(name = "categoryType")
    private String categoryType;

//    @OneToMany
//    private List<Vehicle> vehicle;

}
