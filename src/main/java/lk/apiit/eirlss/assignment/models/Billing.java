package lk.apiit.eirlss.assignment.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
@Table(name = "Billing")
public class Billing {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "billingID")
    private String billingID;

    @OneToOne
    @JoinColumn(name = "booking_bookingID", referencedColumnName = "bookingID")
    private Booking booking;

    @Column(name = "amount")
    private float amount;

}
