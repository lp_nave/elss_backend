package lk.apiit.eirlss.assignment.services;

import lk.apiit.eirlss.assignment.repositories.AuthenticationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationService {

    private AuthenticationRepo authenticationRepo;

    @Autowired
    public AuthenticationService(AuthenticationRepo authenticationRepo) {
        this.authenticationRepo = authenticationRepo;
    }
}
