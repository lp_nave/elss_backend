package lk.apiit.eirlss.assignment.services;

import lk.apiit.eirlss.assignment.repositories.BillingRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BillingService {

    private BillingRepo billingRepo;

    @Autowired
    public BillingService(BillingRepo billingRepo) {
        this.billingRepo = billingRepo;
    }
}
